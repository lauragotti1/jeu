Jeu tamagotchi-like :

Wireframe dans public/assets.

But : faire survivre la chauve-souris le plus longtemps.


On choisit le nom de la chauve-souris (obligatoire) et on peut choisir une couleur de console differente, j'ai mis des choix predefinis pour pas mettre une couleur pas contrastée avec le texte vert.

La chauve-souris est animée avec des gifs. 

Elle perd 1 point de vie (hunger ou happiness, 2 fois plus de chance de hunger) toutes les 10 secondes et si y'a une des 2 barres de vie à 0 c'est game over.

Quand on clique sur feed ça ajoute un point dans hunger (si y'en a moins de 10) et gif different d'animation + son.

Quand on clique sur play ça lance un mini jeu random entre pierre-feuille-sciseaux et jeu ou faut sauter, ça ajoute un point de vie à happiness (même si on perd le mini-jeu).

Tout le long du jeu on voit la durée de vie en minute, quand game over on voit son score et si on joue plus d'une fois le meilleur score du localstorage est affiché. 


https://bat-game.netlify.app/ 



