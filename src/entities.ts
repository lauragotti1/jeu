export interface Pet {
    name:string;
    age:number;
    hunger:number;
    happiness:number;
}