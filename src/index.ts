import { Pet } from "./entities";
import happygreenbat from "/public/assets/happygreenbat.gif";
import greenbat from "/public/assets/greenbat.gif";

const idDiv = document.querySelector<HTMLDivElement>('#petID');
const hungerDiv = document.querySelector<HTMLDivElement>('#hunger');
const happinessDiv = document.querySelector<HTMLDivElement>('#happiness');
const feedButton = document.querySelector<HTMLButtonElement>('#feed');
const playButton = document.querySelector<HTMLButtonElement>('#play');
const form = document.querySelector<HTMLInputElement>('form');
const nameChoice = document.querySelector<HTMLInputElement>('#nameChoice');
const colorChoice = document.querySelector<HTMLSelectElement>('#colorChoice');
const petContainer = document.querySelector<HTMLDivElement>('#petContainer');
const startingScreen = document.querySelector<HTMLDivElement>('#startingScreen');
const gameoverScreen = document.querySelector<HTMLDivElement>('#gameOver');
const results = document.querySelector<HTMLDivElement>('#results');
const restartButton = document.querySelector<HTMLButtonElement>('#restart');
const rpsScreen = document.querySelector<HTMLButtonElement>('#rpsScreen');
const petImg = document.querySelector<HTMLImageElement>('#pet');
const containerColor = document.querySelector<HTMLDivElement>(".container");
const runnerScreen = document.querySelector<HTMLDivElement>('#runnerScreen');
const audio = document.querySelector<HTMLAudioElement>('#audio');

let pet: Pet = {
    name: 'Pet',
    age: 0,
    hunger: 10,
    happiness: 10
}

//écran de démarrage : choix du nom, de la couleur de la console, start cache cet écran et fait apparaitre le jeu,  fonction perte de point et compteur d'age lancé
form?.addEventListener('submit', (event) => {
    event.preventDefault();
    containerColor.style.backgroundColor = colorChoice.value;
    pet.name = nameChoice.value;
    petContainer.classList.remove('hide')
    startingScreen.classList.add('hide');
    displayPetData()
    setInterval(losePoints, 10000); //10000 = toutes les 10 secondes
    setInterval(ageCounter, 60000); //l'age en minute
})

/**met à jour les données affichées et lance gameover*/
function displayPetData() {
    idDiv.innerHTML = `${pet.name} / Age : ${pet.age} min`;
    if (pet.hunger > 0 && pet.happiness > 0) {
        hungerDiv.innerHTML = "♥ : " + "|".repeat(pet.hunger);
        happinessDiv.innerHTML = "☺ : " + "|".repeat(pet.happiness);
    }
    gameOver();
}

/**fonction perte de points automatique, aleatoire entre happiness et hunger mais 2 fois + de chance de hunger */
function losePoints() {
    if (isGameOn == true) {
        let random = Math.floor(Math.random() * 3);
        if (random == 0) {
            pet.happiness--;
        } else {
            pet.hunger--;
        }
        displayPetData();
    }
}

//boutons feed et play
feedButton.addEventListener('click', () => {
    if (pet.hunger < 10) {
        audio.play();
        pet.hunger++;
        displayPetData();
        //anim image
        petImg.src = happygreenbat;
        setTimeout(changeImage, 3000);
        function changeImage() {
            petImg.src = greenbat;
        }
    }
})

playButton.addEventListener('click', () => {
    if (pet.happiness < 10) {
        pet.happiness++;
        displayPetData();
    }
    petContainer.classList.add('hide');
    //jeu random 
    let randomGame = Math.floor(Math.random() * 2);
    if (randomGame == 1) {
    runnerScreen.classList.remove('hide');
    runnerMain(10);
    } else {
        rpsScreen.classList.remove('hide');
        rpsPetName_span.innerHTML = pet.name;
    }
})

let isGameOn = true;
/**fin du jeu quand hunger ou happiness a 0, cache pet, montre game over*/
function gameOver() {
    if (pet.hunger == 0 || pet.happiness == 0) {
        isGameOn = false;
        console.log('gameover');
        petContainer.classList.add('hide');
        rpsScreen.classList.add('hide');
        runnerScreen.classList.add('hide');
        gameoverScreen.classList.remove('hide');
        results.innerHTML = `${pet.name} lived here for ${pet.age} minute(s)`;
        restartButton.addEventListener('click', () => {
            window.location.reload();
        });
        //best score
        let bestScore = localStorage.getItem('bestScore');
        if (Number(bestScore) < pet.age) {
            localStorage.setItem('bestScore', JSON.stringify(pet.age))
        }
        if (bestScore != null) {
            const bestScoreP = document.createElement('p');
            results.appendChild(bestScoreP);
            bestScoreP.innerHTML = `your best score is ${bestScore} minute(s)`;
        }
    }
}

/**incremente age quand jeu en cours*/
function ageCounter() {
    if (isGameOn == true) {
        pet.age++;
        displayPetData();
    }
}

//ROCK PAPER SCISSORS GAME 
let rpsUserScore = 0;
let rpsCompScore = 0;
const rpsScores_div = document.querySelector<HTMLDivElement>('#rpsScores');
const rpsUserScore_span = document.querySelector<HTMLSpanElement>('#rpsUserScore');
const rpsCompScore_span = document.querySelector<HTMLSpanElement>('#rpsCompScore');
const rock_button = document.querySelector<HTMLButtonElement>('#rock');
const paper_button = document.querySelector<HTMLButtonElement>('#paper');
const scissors_button = document.querySelector<HTMLButtonElement>('#scissors');
const rpsResult = document.querySelector<HTMLParagraphElement>('#rpsResult');
const rpsRules = document.querySelector<HTMLParagraphElement>('#rpsRules');
const backButton = document.querySelector<HTMLButtonElement>('#backButton');
const rpsPetName_span = document.querySelector<HTMLSpanElement>('#rpsPetName');

/** return un choix random du computer*/
function rpsGetComputerChoice() {
    const choices = ['rock', 'paper', 'scissors'];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices[randomNumber];
}

/**compare les choix joueur et comp en les concatenant et appelle fonction win ou lose ou draw en fonction*/
function rpsGame(userChoice: string) {
    const computerChoice = rpsGetComputerChoice();
    switch (userChoice + computerChoice) {
        case "rockscissors":
        case "paperrock":
        case "scissorspaper":
            rpsWin(userChoice, computerChoice);
            break;
        case "rockpaper":
        case "paperscissors":
        case "scissorsrock":
            rpsLose(userChoice, computerChoice);
            break;
        case "rockrock":
        case "paperpaper":
        case "scissorsscissors":
            rpsDraw(userChoice, computerChoice);
            break;
    }

}

/**call rpsGame avec le choix cliqué en argument*/
function rpsMain() {
    rock_button.addEventListener('click', () => {
        rpsGame("rock");
    })

    paper_button.addEventListener('click', () => {
        rpsGame("paper");
    })

    scissors_button.addEventListener('click', () => {
        rpsGame("scissors");
    })

}

/**si le joueur gagne, incremente score et montre les choix de chacun*/
function rpsWin(userChoice: string, computerChoice: string) {
    rpsUserScore++;
    rpsUserScore_span.innerHTML = String(rpsUserScore);
    rpsCompScore_span.innerHTML = String(rpsCompScore);
    rpsResult.innerHTML = `${userChoice} wins against ${computerChoice}`;
    endRPS();
}

/**si le computer gagne, incremente score et montre les choix de chacun*/
function rpsLose(userChoice: string, computerChoice: string) {
    rpsCompScore++;
    rpsUserScore_span.innerHTML = String(rpsUserScore);
    rpsCompScore_span.innerHTML = String(rpsCompScore);
    rpsResult.innerHTML = `${userChoice} loses to ${computerChoice}`;
    endRPS();
}

function rpsDraw(userChoice: string, computerChoice: string) {
    rpsResult.innerHTML = `It's a draw`;
}

/**pour montrer la fin du jeu quand un des deux atteint 3 pts, reset les données du jeu pour y rejouer*/
function endRPS() {
    if (rpsUserScore === 3 || rpsCompScore === 3) {

        //display none ou affiche 
        rpsScores_div.classList.add('hide')
        rpsResult.classList.add('hide')
        rock_button.classList.add('hide')
        paper_button.classList.add('hide')
        scissors_button.classList.add('hide')
        backButton.classList.remove('hide')

        //affiche si on a perdu ou gagné
        if (rpsUserScore === 3) {
            rpsRules.innerHTML = `congrats! you won the game against ${pet.name}`
        } else {
            rpsRules.innerHTML = `you lost the game against ${pet.name}`
        }

        //event bouton go back
        backButton.addEventListener('click', () => {
            petContainer.classList.remove('hide');
            rpsScreen.classList.add('hide');
            //reset le jeu
            rpsUserScore = 0;
            rpsCompScore = 0;
            rpsScores_div.classList.remove('hide')
            rpsResult.classList.remove('hide')
            rock_button.classList.remove('hide')
            paper_button.classList.remove('hide')
            scissors_button.classList.remove('hide')
            backButton.classList.add('hide')
            rpsResult.innerHTML = "";
            rpsUserScore_span.innerHTML = String(rpsUserScore);
            rpsCompScore_span.innerHTML = String(rpsCompScore);
            rpsRules.innerHTML = "First to 3 points win!";
        })
    }
}

//lance jeu rps
rpsMain();

//RUNNER GAME 
const character = document.querySelector<HTMLDivElement>('#charac');
const obstacle = document.querySelector<HTMLDivElement>('#obstacle');
const jumpButton = document.querySelector<HTMLButtonElement>('#jump');
const runnerBack = document.querySelector<HTMLButtonElement>('#runnerBackButton');
const runnerBlackScreen = document.querySelector<HTMLDivElement>('#runnerBlackScreen');
const runnerTimer_span = document.querySelector<HTMLSpanElement>('#runnerTimer');

/**
 * fonction principale du runner, collision, chrono, lance gameover
 * @param time nombre de secondes durée jeu
 */
function runnerMain(time: number) {
    let counter = setInterval(chrono, 1000);
     //si joueur perd (à la collision), dans un setinterval qui verifie tous les 100ms la position
    let colInterval = setInterval(() => {
        let characTop = parseInt(window.getComputedStyle(character).getPropertyValue('top'));
        let obstacleLeft = parseInt(window.getComputedStyle(obstacle).getPropertyValue('left'));
        if (obstacleLeft < 32 && characTop >= 160) {
            console.log('collision');
            runnerGO('lost');
            clearInterval(counter);
            clearInterval(colInterval);
        }
    }, 100);
    function chrono() {
        runnerBack.addEventListener('click', () => {
            clearInterval(counter);
            clearInterval(colInterval);

        });
        runnerTimer_span.innerHTML = String(time);
        time--;
        //si joueur gagne en survivant 10s
        if (time < 0) {
            clearInterval(counter);
            clearInterval(colInterval);
            runnerGO('won');
        }
    
    }
}

jumpButton.addEventListener('click', () => {
    character.classList.add('jumpanim');
    setTimeout(() => { character.classList.remove('jumpanim') }, 500)
});

//creation element message game over
const runnerP = document.createElement('p');
runnerP.classList.add('hide');
runnerBlackScreen.appendChild(runnerP);
/**
 * game over jeu runner
 * @param result = lost ou win
 */
function runnerGO(result: string) {
    runnerP.innerHTML = `you ${result}, go back to ${pet.name}`;
    obstacle.classList.add('hide');
    character.classList.add('hide');
    jumpButton.classList.add('hide');
    runnerBack.classList.remove('hide');
    runnerP.classList.remove('hide');
}

runnerBack.addEventListener('click', () => {
    petContainer.classList.remove('hide');
    runnerScreen.classList.add('hide');
    //reset le jeu pour le relancer
    obstacle.classList.remove('hide');
    character.classList.remove('hide');
    jumpButton.classList.remove('hide');
    runnerBack.classList.add('hide');
    runnerP.classList.add('hide');
})
